package innometrics.agent.jenkins;

import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.model.Job;
import com.offbytwo.jenkins.model.JobWithDetails;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class JobService {


    public List<JobWithDetails> getJobs() throws URISyntaxException, IOException {
        JenkinsServer jenkins = new JenkinsServer(new URI("http://18.191.182.175:8888"), "agent", "agent");
        Map<String, Job> jobs = jenkins.getJobs();



        return jobs.values().stream().map( j -> {
            try {
                return j.details();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }).collect(Collectors.toList());
    }
}

