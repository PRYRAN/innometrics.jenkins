package innometrics.agent.jenkins;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.offbytwo.jenkins.model.Build;
import com.offbytwo.jenkins.model.Job;
import com.offbytwo.jenkins.model.JobWithDetails;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping(value = "")
@RestController
public class JobController {

    @Autowired
    JobService jobService;
    @Autowired
    ObjectMapper objectMapper;


    @GetMapping
    public List<JobDTO> getJobs() throws IOException, URISyntaxException {


        return jobService.getJobs().stream().map(
                j -> {
                    JobDTO jobDTO = new JobDTO();
                    BeanUtils.copyProperties(j,jobDTO);
                    try {
                        jobDTO.setFirstBuild(j.getFirstBuild().details().getTimestamp());
                        jobDTO.setLastBuild(j.getLastBuild().details().getTimestamp());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return jobDTO;
                }
        ).collect(Collectors.toList());

    }
}
