package innometrics.agent.jenkins;

import com.offbytwo.jenkins.model.Build;
import lombok.Data;

@Data
public class JobDTO {

    private String description;
    private String displayName;
    private boolean buildable;
    private Long firstBuild;
    private Long lastBuild;


}
